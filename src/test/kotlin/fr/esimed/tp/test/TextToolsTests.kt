package fr.esimed.tp.test

import fr.esimed.tp.levenshtein
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.function.Executable

class TextToolsTests
{
    @Test
    fun levenshteinDistanceTest()
    {
        Assertions.assertAll(
            Executable { Assertions.assertEquals(5, levenshtein("Arnaud", "Julien"))},
            Executable { Assertions.assertEquals(1, levenshtein("Julie", "Julien"))}
        )
    }

}
