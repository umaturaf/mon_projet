package fr.esimed.tp

fun levenshtein(a : String, b :String) : Int
{
    val aLength = a.length
    val bLength = b.length

    var m = Array(aLength+1,{IntArray(bLength+1)})
    var c = Array(aLength,{IntArray(bLength)})

    for(i in 1 until aLength+1)
    {
        m[i][0] = i
    }

    for (j in 1 until bLength+1)
    {
        m[0][j] = j
    }

    for(s in 0 until aLength)
    {
        for (j in 0 until bLength)
        {
            if(a[s] == b[j]) { c[s][j] = 0 }
            else { c[s][j] = 1 }
        }
    }

    for(i in 1 until aLength+1)
    {
        for(j in 1 until bLength+1)
        {
            val suppression = (m[i-1][j]) + 1
            val insertion = (m[i][j-1]) + 1
            val substitution = (m[i-1][j-1]) + c[i-1][j-1]

            m[i][j] = Math.min(Math.min(insertion,suppression),substitution)
        }
    }

//    for (i in 0 until aLength)
//    {
//        for (j in 0 until bLength)
//        {
//            print("${c[i][j]}|")
//        }
//        println("\r")
//    }
//
//    println("")
//
//    for (i in 0 until aLength+1)
//    {
//        for (j in 0 until bLength+1)
//        {
//            print("${m[i][j]}|")
//        }
//        println("\r")
//    }

    return m[ aLength ][ bLength ]
}
