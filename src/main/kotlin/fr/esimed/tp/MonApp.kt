package fr.esimed.tp

fun main()
{
    println("hello world !")
    var res1 = levenshtein("Arnaud","Julien")
    var res2 = levenshtein("Julie","Julien")
    var res3 = levenshtein("niche","chiens")
    println("distance lavenshtein entre Arnaud et Julien : $res1")
    println("distance lavenshtein entre Julie et Julien : $res2")
    println("distance lavenshtein entre niche et chiens : $res3")
}